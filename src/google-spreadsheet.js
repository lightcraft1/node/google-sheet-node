'use strict';

const { google } = require('googleapis');

/**
 * Google Sheets API module
 * @class PcsSreSpreadsheet
 */
class GoogleSpreadSheet {
	
	/**
	 * Class constructor
	 * @constructor
	 * @param {object} keys - Google Auth credentials
	 * @param {string} spreadsheetId 
	 */
	constructor(keys, spreadsheetId) {
		if (!keys || !keys.private_key || !keys.client_email) {
			throw new Error('Google API credentials not found.');
		}

		if (!spreadsheetId) {
			throw new Error('spreadsheet id is not specified');
		}

		this.spreadsheetId = spreadsheetId;
		this.keys = keys;
	}

	/**
	 * Initializes connection configuration
	 */
	async init() {
		this.auth = await this.getAuth(this.keys);
		this.spreadsheet = await this.getGoogleSheet(this.auth);
	}

	/**
	 * Gets auth client
	 * @param {object} keys - Google Auth credentials
	 * @returns auth client 
	 */
	async getAuth(keys) {
		let auth = new google.auth.GoogleAuth({
			credentials: {
				client_email: keys.client_email,
				private_key: keys.private_key,
			},
			scopes: 'https://www.googleapis.com/auth/spreadsheets',
		});
		const client = await auth.getClient();
		return client;
	}

	/**
	 * Gets google sheet object
	 * @param {object} auth 
	 * @returns 
	 */
	async getGoogleSheet(auth) {
		const googleSheet = google.sheets({
			version: 'v4',
			auth: auth,
		});
		return googleSheet;
	}

	/**
	 * Gets spreadsheet values for a given range
	 * @param {string} spreadsheetRange
	 * e.g. Sheet1!A1:R684 (<sheet name>!<range>)
	 * @returns {Array} spreadsheet values
	 */
	async getSpreadsheetRowsByRange(spreadsheetRange) {
		await this.init();
		if (!spreadsheetRange) {
			throw new Error('spreadsheet range not found.');
		}
		const rows = await this.spreadsheet.spreadsheets.values.get({
			auth: this.auth,
			spreadsheetId: this.spreadsheetId,
			range: spreadsheetRange,
		});
		return rows.data.values;
	}

	/**
	 * Gets the metadata of the spreadsheet
	 * @returns {Array}
	 */
	async getMetaData() {
		await this.init();
		const metaData = await this.spreadsheet.spreadsheets.get({
			auth: this.auth,
			spreadsheetId: this.spreadsheetId,
		});
		return metaData;
	}

	/**
	 * Writes data to worksheet
	 * @param {*} range 
	 * @param {*} values 
	 */
	async writeToWorkSheet(range, values) {
		await this.init();
		const requestBody = { values };
		this.spreadsheet.spreadsheets.values.update({
			auth: this.auth,
			spreadsheetId: this.spreadsheetId,
			range,
			valueInputOption: 'RAW',
			requestBody,
		});
	}

	/**
	 * creates worksheet by name
	 * @param {string} sheetName 
	 */
	async createWorksheetByName(sheetName) {
		await this.init();
		const requestBody = {
			requests: [
				{
					addSheet: {
						properties: {
							title: sheetName,
						},
					},
				},
			],
		};

		let res = {};
		try {
			res = await this.spreadsheet.spreadsheets.batchUpdate({
				auth,
				spreadsheetId: this.spreadsheetId,
				requestBody,
			});
		} catch (e) {
			console.error(`Unable to create worksheet ${e.message}`);
		}
	}
}

module.exports = GoogleSpreadSheet;
